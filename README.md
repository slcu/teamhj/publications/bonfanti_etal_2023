## Introduction

Code developed for the data analysis for the paper

Alessandra Bonfanti, Euan Thomas Smithers, Matthieu Bourdon, Alex Guyon, Philip Carella, Ross Carter, Raymond Wightman, Sebastian Schornack, Henrik Jönsson, and Sarah Robinson (2023)
Stiffness transitions in new walls post-cell division differ between Marchantia polymorpha gemmae and Arabidopsis thaliana leaves, PNAS 120 (41) e2302985120.

https://doi.org/10.1073/pnas.2302985120

## Main repository

The main repository (cloned here) is https://github.com/alebonfanti/plant-cell-division-growth

The original data for the paper is available from https://doi.org/10.5281/zenodo.7685356

## Scripts

- morphographx_to_tissue.jl: it takes in input a triangular mesh from MorphographX and it returns an input file for Tissue (.init) (computational software for plant simultion).
- cell_wall_growth.jl: it takes in input two .init files (two different time points) and a file containing the parent lables between the two time point segmentations from MorphographX. It returns the growth map of the cell walls.
- shortestPath_beforeDivision_with PGD.jl: it takes in input two .init files and the labels of the dividing cells. It compute the shortest path for the dividing cells and compare it with the actual plane of division.
- ImageJ analysis macro for flourescence analysis (Dr Matthieu Bourdon): macro code for signal quantification in immunostaining images (highly dividing zone vs slower dividing zone). 






